create table teacher
(
    id         serial primary key,
    first_name varchar(20) not null default 'FIRST_NAME',
    last_name  varchar(20) not null default 'LAST_NAME',
    experience integer     not null default 0
);

create table course
(
    id         serial primary key,
    name       varchar(20) not null default 'NAME',
    start  varchar(20) not null default 'START',
    end     varchar(20) not null default 'END',
    teacher_id integer,
    foreign key (teacher_id) references "postgres@localhost"teacher (id)
);

create table lesson
(
    id          serial primary key,
    name        varchar(20) not null default 'NAME',
    day_of_week varchar(20) not null default 'DAY_OF_WEEK',
    time        varchar(33) not null default 'TIME',
    course_id   integer,
    foreign key (course_id) references course (id)
);

create table student
(
    id         serial primary key,
    first_name varchar(20) not null default 'FIRST_NAME',
    last_name  varchar(20) not null default 'LAST_NAME',
    group_number   integer     not null
);

create table courseStudent
(
    course_id  integer,
    student_id integer,
    foreign key (course_id) references course (id),
    foreign key (student_id) references student (id)
);

create table courseTeacher
(
    course_id  integer,
    teacher_id integer,
    foreign key (course_id) references course (id),
    foreign key (teacher_id) references teacher (id)
);

insert into student(first_name, last_name, group_number)
values ('Марсель','Басыров',5)
insert into student(first_name, last_name, group_number)
values ('Дмитрий','Дистров',5)
insert into student(first_name, last_name, group_number)
values ('Василий','Евдокимов',3)
insert into student(first_name, last_name, group_number)
values ('Аркадий','Мясницкий',2)
insert into student(first_name, last_name, group_number)
values ('Александр','Воронин',2)
insert into student(first_name, last_name, group_number)
values ('Игорь','Назыров',1)



insert into teacher(first_name, last_name, experience)
values ('Александр','Симпл',32)
insert into teacher(first_name, last_name, experience)
values ('Валерий','Бит',3)
insert into teacher(first_name, last_name, experience)
values ('Денис','Электроник',15)
insert into teacher(first_name, last_name, experience)
values ('Кирилл','Бумыч',7)
insert into teacher(first_name, last_name, experience)
values ('Илья','Перфекто',18)
insert into teacher(first_name, last_name, experience)
values ('Евгений','Райз',55)

insert into course(name,start,end,teacher_id)
values ('Психология','05.06.2018','06.12.2018',1)
insert into course(name,start,end,teacher_id)
values ('Философия','22.06.2021','15.12.2021',2)
insert into course(name,start,end,teacher_id)
values ('Информатика','18.01.2019','01.01.2020',4)
insert into course(name,start,end,teacher_id)
values ('Математика','12.02.2022','07.03.2023',5)


insert into courseStudent(student_id, course_id)
VALUES (1, 3);
insert into courseStudent(student_id, course_id)
VALUES (2, 4);
insert into courseStudent(student_id, course_id)
VALUES (3, 1);
insert into courseStudent(student_id, course_id)
VALUES (2, 4);
insert into courseStudent(student_id, course_id)
VALUES (5, 5);
insert into courseStudent(student_id, course_id)
VALUES (4, 3);


import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.jdbc.core.RowMapper;

import Models.Student;
import Models.Course;

import javax.sql.DataSource;
import java.sql.*;
import java.util.*;
import java.util.function.Function;

public class CourseRepositoryJdbcTemplateImpl implements CourseRepository {

    private static final String SQL_SELECT_ALL = "select * from course order by id";
    private static final String SQL_SELECT_ALL_BY_NAME = "select * from course where name = ?";
    private static final String SQL_SELECT_BY_ID = "select * from course where id = ?";
    private static final String SQL_UPDATE_BY_ID = "update course set name = ?, start = ?, end = ?, teacher_id = ? where id = ?";
    private static final String SQL_INSERT = "insert into course(name, start, end, teacher_id) values(?, ?, ?, ?)";
    private static final String SQL_DELETE_BY_ID = "delete  from course where id = ?";
    private static final String SQL_DELETE_COURSE = "delete from course where course_id = ?";
    private static final String SQL_INSERT_STUDENT_TO_COURSE = "insert into student_on_course(course_id, student_id) values (?, ?)";


    private JdbcTemplate jdbcTemplate;
    public CourseRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
    private final RowMapper<Course> courseRowMapper = (row, rowNumber) -> {
        int id = row.getInt("course_id");
        String name = row.getString("course_name");
        String start = row.getString("start");
        String end = row.getString("end");
        Integer teacherId = row.getInt("teacher_id");

        Course course = new Course(id, name,start ,end, teacherId, null);
        course.setStudents(new ArrayList<>());
        return course;
    };

    private final ResultSetExtractor<Map<Integer, List<Student>>> courseStudentsResultSetExtractor = resultSet -> {
        Map<Integer, List<Student>> data = new LinkedHashMap<>();
        while (resultSet.next()) {
            Integer courseId = resultSet.getInt("course_id");
            data.putIfAbsent(courseId, new ArrayList<Student>());
            Integer studentId = resultSet.getInt("student_id");
            String firstName = resultSet.getString("first_name");
            String lastName = resultSet.getString("last_name");
            Integer groupId = resultSet.getInt("group_id");
            Student s = new Student(studentId, firstName, lastName, groupId, null);
            data.get(courseId).add(s);
        }
        return data;
    };

    private final ResultSetExtractor<List<Course>> courseWithStudentsResultSetExtractor = resultSet -> {
        Map<Course, List<Student>> data = new LinkedHashMap<>();
        while (resultSet.next()) {
            Integer courseId = resultSet.getInt("course_id");
            String name = resultSet.getString("course_name");
            String start = resultSet.getString("start");
            String end = resultSet.getString("end");
            Integer teacherId = resultSet.getInt("teacher_id");
            Course course = new Course(courseId, name, start,end, teacherId, null);
            data.putIfAbsent(course, new ArrayList<Student>());
            Integer studentId = resultSet.getInt("student_id");
            String firstName = resultSet.getString("first_name");
            String lastName = resultSet.getString("last_name");
            Integer groupNumber = resultSet.getInt("group_number");
            Student s = new Student(studentId, firstName, lastName, groupNumber, null);
            data.get(course).add(s);
        }
        List<Course> result = new ArrayList<>();
        for(Map.Entry<Course, List<Student>> e : data.entrySet()){
            Course course = e.getKey();
            List<Student> studs = e.getValue();
            course.setStudents(studs);
            result.add(course);
        }
        return result;
    };

    @Override
    public List<Course> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, courseWithStudentsResultSetExtractor, null);
    }

    @Override
    public List<Course> findAllByName(String name) {
        return jdbcTemplate.query(SQL_SELECT_ALL_BY_NAME, courseWithStudentsResultSetExtractor, name);
    }

    @Override
    public Optional<Course> findById(Integer id) {
        List<Course> res = jdbcTemplate.query(SQL_SELECT_BY_ID, courseWithStudentsResultSetExtractor, id);
        if(res.size()==1){
            return Optional.of(res.get(0));
        }else{
            return Optional.empty();
        }
    }

    @Override
    public void save(Course course) {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(connection -> {
            PreparedStatement statement = connection.prepareStatement(SQL_INSERT, new String[] {"course_id"});

            statement.setString(1, course.getName());
            statement.setString(2, course.getStart());
            statement.setString(3, course.getEnd());
            statement.setInt(4, course.getTeacherId().getId());

            return statement;
        }, keyHolder);

        course.setId(keyHolder.getKey().intValue());

    }

    @Override
    public void update(Course course) {
        jdbcTemplate.update(SQL_UPDATE_BY_ID, course.getName(),course.getStart(),course.getEnd(),course.getTeacherId(),course.getId());
        jdbcTemplate.update(SQL_DELETE_BY_ID, course.getId());
        for(Student student : course.getStudents()){
            jdbcTemplate.update(SQL_INSERT_STUDENT_TO_COURSE,course.getId(),student.getId());
        }
    }

    @Override
    public void delete(Course course) {
        jdbcTemplate.update(SQL_DELETE_BY_ID, course.getId());
        jdbcTemplate.update(SQL_DELETE_COURSE,course.getId());
    }
}

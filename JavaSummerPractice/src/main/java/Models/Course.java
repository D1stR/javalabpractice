package Models;
import java.util.List;
public class Course {
    private Integer id;
    private String name;
    private String start;
    private String end;
    private Teacher teacherId;
    private List<Student> students;

    public Course(Integer id, String name, String start, String end, Integer teacherId, List<Student> students) {
        this.id = id;
        this.name = name;
        this.start = start;
        this.end = end;
        this.teacherId = teacherId;
        this.students = students;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public Teacher getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Teacher teacherId) {
        this.teacherId = teacherId;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", start='" + start + '\'' +
                ", end='" + end + '\'' +
                ", teacherId=" + teacherId +
                ", students=" + students +
                '}';
    }
}
